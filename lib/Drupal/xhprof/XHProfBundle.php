<?php

/**
 * @file
 * Definition of Drupal\xhprof\XHProfBundle.
 */

namespace Drupal\xhprof;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * The bundle for xhprof.module.
 */
class XHProfBundle extends Bundle
{
  public function build(ContainerBuilder $container) {
    $container->register('xhprof.xhprof_subscriber', 'Drupal\xhprof\XHProfSubscriber')
      ->addTag('event_subscriber');
  }
}