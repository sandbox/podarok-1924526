<?php

/**
 * @file
 * Definition of Drupal\xhprof\EventSubscriber.
 */

namespace Drupal\xhprof\EventSubscriber;

// use Symfony\Component\HttpKernel\KernelEvents;
// use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class XHProfSubscriber implements EventSubscriberInterface {

  /**
   * A simple kernel listener method.
   */
  public function onKernelRequestTest() {
    drupal_set_message(t('My event subscriber fired!'));
  }

  /**
   * Registers methods as kernel listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('onKernelRequestTest', 100);
    return $events;
  }

}
