<?php
/**
 * @file
 * Definition of Drupal\xhprof\XHProfSubscriber.
 */

namespace Drupal\xhprof;

use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Config\StorageDispatcher;
use Symfony\Component\HttpKernel\Event;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\Core\Language\LanguageManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Implements XHProfSubscriber
 */
class XHProfSubscriber implements EventSubscriberInterface {


  /**
   * @param Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Event to process.
   */
  public function XHProfLoad(GetResponseEvent $event) {
    // Initialize XHProf.
    if (xhprof_xhprof_enable()) {
      drupal_set_message('XHProf: subscribed');
      drupal_register_shutdown_function('xhprof_shutdown');
    }
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('XHProfLoad', 999);
    return $events;
  }
}
